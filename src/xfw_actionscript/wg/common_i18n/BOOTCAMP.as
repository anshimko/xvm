package
{
    public class BOOTCAMP extends Object
    {

        public static const WELLCOME_BOOTCAMP:String = "#bootcamp:wellcome/bootcamp";

        public static const WELLCOME_BOOTCAMP_DESCRIPTION:String = "#bootcamp:wellcome/bootcamp/description";

        public static const WELLCOME_BOOTCAMP_WAIT:String = "#bootcamp:wellcome/bootcamp/wait";

        public static const WELLCOME_BOOTCAMP_REFERRAL:String = "#bootcamp:wellcome/bootcamp/referral";

        public static const FINISH_BOOTCAMP_REFERRAL:String = "#bootcamp:finish/bootcamp/referral";

        public static const REQUEST_BOOTCAMP_RETURN:String = "#bootcamp:request/bootcamp/return";

        public static const REQUEST_BOOTCAMP_START:String = "#bootcamp:request/bootcamp/start";

        public static const REQUEST_BOOTCAMP_FINISH:String = "#bootcamp:request/bootcamp/finish";

        public static const BTN_SKIPVIDEO:String = "#bootcamp:btn/skipVideo";

        public static const BTN_SELECT:String = "#bootcamp:btn/select";

        public static const BTN_CONTINUE:String = "#bootcamp:btn/continue";

        public static const BTN_CANCEL:String = "#bootcamp:btn/cancel";

        public static const BTN_CONTINUE_PREBATTLE:String = "#bootcamp:btn/continue/prebattle";

        public static const GAME_MODE:String = "#bootcamp:game/mode";

        public static const BTN_TUTORIAL_START:String = "#bootcamp:btn/tutorial/start";

        public static const BTN_TUTORIAL_SKIP:String = "#bootcamp:btn/tutorial/skip";

        public static const BTN_TUTORIAL_CLOSE:String = "#bootcamp:btn/tutorial/close";

        public static const PROMO_VEHICLEBUYVIEW:String = "#bootcamp:promo/vehicleBuyView";

        public static const COST_MESSAGEVIEWBUY:String = "#bootcamp:cost/messageViewBuy";

        public static const AWARD_OPTIONS:String = "#bootcamp:award/options";

        public static const AWARD_OPTIONS_TITLE:String = "#bootcamp:award/options/title";

        public static const AWARD_OPTIONS_NATION_US:String = "#bootcamp:award/options/nation/us";

        public static const AWARD_OPTIONS_NATION_GE:String = "#bootcamp:award/options/nation/ge";

        public static const AWARD_OPTIONS_NATION_USSR:String = "#bootcamp:award/options/nation/ussr";

        public static const AWARD_OPTIONS_NAME_US:String = "#bootcamp:award/options/name/us";

        public static const AWARD_OPTIONS_DESCRIPTION_US:String = "#bootcamp:award/options/description/us";

        public static const AWARD_OPTIONS_NAME_GE:String = "#bootcamp:award/options/name/ge";

        public static const AWARD_OPTIONS_DESCRIPTION_GE:String = "#bootcamp:award/options/description/ge";

        public static const AWARD_OPTIONS_NAME_USSR:String = "#bootcamp:award/options/name/ussr";

        public static const AWARD_OPTIONS_DESCRIPTION_USSR:String = "#bootcamp:award/options/description/ussr";

        public static const HINT_CAMERA_CONTROLS:String = "#bootcamp:hint/camera/controls";

        public static const HINT_MOVE:String = "#bootcamp:hint/move";

        public static const HINT_MOVE_TURRET:String = "#bootcamp:hint/move/turret";

        public static const HINT_SHOOT:String = "#bootcamp:hint/shoot";

        public static const HINT_SNIPER:String = "#bootcamp:hint/sniper";

        public static const HINT_ADVANCED_SNIPER_MAIN:String = "#bootcamp:hint/advanced/sniper/main";

        public static const HINT_ADVANCED_SNIPER_BEFORE:String = "#bootcamp:hint/advanced/sniper/before";

        public static const HINT_MESSAGE_AVOID:String = "#bootcamp:hint/message/avoid";

        public static const HINT_REPAIR_TRACK:String = "#bootcamp:hint/repair/track";

        public static const HINT_USE_EXTINGUISHER:String = "#bootcamp:hint/use/extinguisher";

        public static const HINT_HEAL_CREW:String = "#bootcamp:hint/heal/crew";

        public static const HINT_SNIPER_ON_DISTANCE_MAIN:String = "#bootcamp:hint/sniper/on/distance/main";

        public static const HINT_SNIPER_ON_DISTANCE_EXIT:String = "#bootcamp:hint/sniper/on/distance/exit";

        public static const HINT_NO_MOVE:String = "#bootcamp:hint/no/move";

        public static const HINT_ALLY_SHOOT:String = "#bootcamp:hint/ally/shoot";

        public static const HINT_TARGET_UNLOCK:String = "#bootcamp:hint/target/unlock";

        public static const HINT_USELESS_CONSUMABLES:String = "#bootcamp:hint/useless/consumables";

        public static const HINT_WAIT_RELOAD:String = "#bootcamp:hint/wait/reload";

        public static const HINT_EXIT_GAME_AREA:String = "#bootcamp:hint/exit/game/area";

        public static const HINT_SHOOT_WHILE_MOVING:String = "#bootcamp:hint/shoot/while/moving";

        public static const HINT_SECONDARY_SNIPER:String = "#bootcamp:hint/secondary/sniper";

        public static const HINT_LOW_HP:String = "#bootcamp:hint/low/hp";

        public static const HINT_MISSION3_PLAYERDETECTED:String = "#bootcamp:hint/mission3/playerdetected";

        public static const HINT_MISSION3_FALLBACK:String = "#bootcamp:hint/mission3/fallback";

        public static const HINT_MISSION3_FLANKENEMIES:String = "#bootcamp:hint/mission3/flankenemies";

        public static const HINT_MISSION3_FOLIAGEINTROA:String = "#bootcamp:hint/mission3/foliageintroa";

        public static const HINT_MISSION3_FOLIAGEINTROB:String = "#bootcamp:hint/mission3/foliageintrob";

        public static const HINT_MISSION3_FLANKINGFAILS:String = "#bootcamp:hint/mission3/flankingfails";

        public static const HINT_MISSION3_FLANKINGFAILS2:String = "#bootcamp:hint/mission3/flankingfails2";

        public static const HINT_MISSION3_FLANKINGWAIT:String = "#bootcamp:hint/mission3/flankingwait";

        public static const HINT_MISSION3_CAPTUREBASE:String = "#bootcamp:hint/mission3/capturebase";

        public static const HINT_MISSION3_CAPTURELOST:String = "#bootcamp:hint/mission3/capturelost";

        public static const HINT_MISSION3_CAPTURETOGETHER:String = "#bootcamp:hint/mission3/capturetogether";

        public static const HINT_MISSION3_CAPTUREHELP:String = "#bootcamp:hint/mission3/capturehelp";

        public static const HINT_MISSION3_CAPTUREINPROGRESS:String = "#bootcamp:hint/mission3/captureinprogress";

        public static const QUEST_TITLE:String = "#bootcamp:quest/title";

        public static const QUEST_NAME:String = "#bootcamp:quest/name";

        public static const QUEST_CONDITION:String = "#bootcamp:quest/condition";

        public static const QUEST_GAMEMODE:String = "#bootcamp:quest/gamemode";

        public static const LOADING_TIP_WASD_HEADER_1:String = "#bootcamp:loading/tip/wasd/header/1";

        public static const LOADING_TIP_WASD_HEADER_2:String = "#bootcamp:loading/tip/wasd/header/2";

        public static const LOADING_TIP_WASD_HEADER_3:String = "#bootcamp:loading/tip/wasd/header/3";

        public static const LOADING_TIP_SNIPER_HEADER_1:String = "#bootcamp:loading/tip/sniper/header/1";

        public static const LOADING_TIP_SNIPER_HEADER_2:String = "#bootcamp:loading/tip/sniper/header/2";

        public static const LOADING_TIP_SNIPER_DESCRIPTION_1:String = "#bootcamp:loading/tip/sniper/description/1";

        public static const LOADING_TIP_SNIPER_DESCRIPTION_2:String = "#bootcamp:loading/tip/sniper/description/2";

        public static const LOADING_TIP_PENETRATION_HEADER_1:String = "#bootcamp:loading/tip/penetration/header/1";

        public static const LOADING_TIP_PENETRATION_HEADER_2:String = "#bootcamp:loading/tip/penetration/header/2";

        public static const LOADING_TIP_PENETRATION_DESCRIPTION_1:String = "#bootcamp:loading/tip/penetration/description/1";

        public static const LOADING_TIP_PENETRATION_DESCRIPTION_2:String = "#bootcamp:loading/tip/penetration/description/2";

        public static const LOADING_TIP_VISIBILITY_HEADER_1:String = "#bootcamp:loading/tip/visibility/header/1";

        public static const LOADING_TIP_VISIBILITY_HEADER_2:String = "#bootcamp:loading/tip/visibility/header/2";

        public static const LOADING_TIP_VISIBILITY_DESCRIPTION_1:String = "#bootcamp:loading/tip/visibility/description/1";

        public static const LOADING_TIP_VISIBILITY_DESCRIPTION_2:String = "#bootcamp:loading/tip/visibility/description/2";

        public static const LOADING_TIP_VISIBILITY_DESCRIPTION_3:String = "#bootcamp:loading/tip/visibility/description/3";

        public static const LOADING_TIP_VISIBILITY_DESCRIPTION_4:String = "#bootcamp:loading/tip/visibility/description/4";

        public static const LOADING_TIP_EQUIPMENT_HEADER_1:String = "#bootcamp:loading/tip/equipment/header/1";

        public static const LOADING_TIP_EQUIPMENT_HEADER_2:String = "#bootcamp:loading/tip/equipment/header/2";

        public static const LOADING_TIP_EQUIPMENT_HEADER_3:String = "#bootcamp:loading/tip/equipment/header/3";

        public static const LOADING_TIP_EQUIPMENT_DESCRIPTION_1:String = "#bootcamp:loading/tip/equipment/description/1";

        public static const LOADING_TIP_EQUIPMENT_DESCRIPTION_2:String = "#bootcamp:loading/tip/equipment/description/2";

        public static const LOADING_TIP_EQUIPMENT_DESCRIPTION_3:String = "#bootcamp:loading/tip/equipment/description/3";

        public static const LOADING_TIP_VICTORY_HEADER_1:String = "#bootcamp:loading/tip/victory/header/1";

        public static const LOADING_TIP_VICTORY_HEADER_2:String = "#bootcamp:loading/tip/victory/header/2";

        public static const LOADING_TIP_VICTORY_DESCRIPTION_1:String = "#bootcamp:loading/tip/victory/description/1";

        public static const LOADING_TIP_VICTORY_DESCRIPTION_2:String = "#bootcamp:loading/tip/victory/description/2";

        public static const LOADING_TIP_CROSSHAIR_HEADER_1:String = "#bootcamp:loading/tip/crosshair/header/1";

        public static const LOADING_TIP_CROSSHAIR_HEADER_2:String = "#bootcamp:loading/tip/crosshair/header/2";

        public static const LOADING_TIP_MODULES_HEADER_1:String = "#bootcamp:loading/tip/modules/header/1";

        public static const LOADING_TIP_MODULES_HEADER_2:String = "#bootcamp:loading/tip/modules/header/2";

        public static const LOADING_TIP_MODULES_HEADER_3:String = "#bootcamp:loading/tip/modules/header/3";

        public static const LOADING_TIP_MODULES_DESCRIPTION_2:String = "#bootcamp:loading/tip/modules/description/2";

        public static const LOADING_TIP_MODULES_DESCRIPTION_3:String = "#bootcamp:loading/tip/modules/description/3";

        public static const PREBATTLE_HINT_SCORE:String = "#bootcamp:prebattle/hint/score";

        public static const PREBATTLE_HINT_HP:String = "#bootcamp:prebattle/hint/hp";

        public static const PREBATTLE_HINT_MODULES:String = "#bootcamp:prebattle/hint/modules";

        public static const PREBATTLE_HINT_CREW:String = "#bootcamp:prebattle/hint/crew";

        public static const PREBATTLE_HINT_MINIMAP:String = "#bootcamp:prebattle/hint/minimap";

        public static const PREBATTLE_HINT_CONSUMABLES:String = "#bootcamp:prebattle/hint/consumables";

        public static const PREBATTLE_HINT_PENETRATION_CHANCE:String = "#bootcamp:prebattle/hint/penetration/chance";

        public static const PREBATTLE_HINT_PENETRATION_CHANCE_HIGH:String = "#bootcamp:prebattle/hint/penetration/chance/high";

        public static const PREBATTLE_HINT_PENETRATION_CHANCE_LOW:String = "#bootcamp:prebattle/hint/penetration/chance/low";

        public static const PREBATTLE_HINT_PENETRATION_HIGH:String = "#bootcamp:prebattle/hint/penetration/high";

        public static const PREBATTLE_HINT_PENETRATION_LOW:String = "#bootcamp:prebattle/hint/penetration/low";

        public static const MESSAGE_VEHICLE_AWARDED_LABEL:String = "#bootcamp:message/vehicle/awarded/label";

        public static const MESSAGE_VEHICLE_AWARDED_TEXT:String = "#bootcamp:message/vehicle/awarded/text";

        public static const MESSAGE_EXTRA_AWARD_OPTIONS:String = "#bootcamp:message/extra/award/options";

        public static const MESSAGE_CREDITS_LABEL:String = "#bootcamp:message/credits/label";

        public static const MESSAGE_CREDITS_TEXT:String = "#bootcamp:message/credits/text";

        public static const MESSAGE_EXPERIENCE_LABEL:String = "#bootcamp:message/experience/label";

        public static const MESSAGE_EXPERIENCE_TEXT:String = "#bootcamp:message/experience/text";

        public static const MESSAGE_UNLOCK_MODULE_BUTTONLABEL:String = "#bootcamp:message/unlock/module/buttonLabel";

        public static const MESSAGE_UNLOCK_MODULE_TITLE:String = "#bootcamp:message/unlock/module/title";

        public static const MESSAGE_UNLOCK_VEHICLE_BUTTONLABEL:String = "#bootcamp:message/unlock/vehicle/buttonLabel";

        public static const MESSAGE_UNLOCK_VEHICLE_TITLE:String = "#bootcamp:message/unlock/vehicle/title";

        public static const MESSAGE_CONFIRMBUYANDINSTALL_MODULE_BUTTONLABEL:String = "#bootcamp:message/confirmBuyAndInstall/module/buttonLabel";

        public static const MESSAGE_CONFIRMBUYANDINSTALL_MODULE_TITLE:String = "#bootcamp:message/confirmBuyAndInstall/module/title";

        public static const MESSAGE_UNLOCK_MODULE_TEXT:String = "#bootcamp:message/unlock/module/text";

        public static const MESSAGE_NEW_MODULE_LABEL:String = "#bootcamp:message/new/module/label";

        public static const MESSAGE_NEW_MODULE_TEXT:String = "#bootcamp:message/new/module/text";

        public static const MESSAGE_UNLOCK_VEHICLE_LABEL:String = "#bootcamp:message/unlock/vehicle/label";

        public static const MESSAGE_TEACHING_TEAM_LABEL:String = "#bootcamp:message/teaching/team/label";

        public static const MESSAGE_TEACHING_TEAM_TEXT:String = "#bootcamp:message/teaching/team/text";

        public static const MESSAGE_UNLOCK_VEHICLE_TEXT:String = "#bootcamp:message/unlock/vehicle/text";

        public static const MESSAGE_SECOND_VEHICLE_TEXT_NATION_0:String = "#bootcamp:message/second/vehicle/text/nation/0";

        public static const MESSAGE_SECOND_VEHICLE_TEXT_NATION_1:String = "#bootcamp:message/second/vehicle/text/nation/1";

        public static const MESSAGE_SECOND_VEHICLE_TEXT_NATION_2:String = "#bootcamp:message/second/vehicle/text/nation/2";

        public static const MESSAGE_SKILLS_AND_PERKS_LABEL:String = "#bootcamp:message/skills/and/perks/label";

        public static const MESSAGE_SKILLS_AND_PERKS_TEXT:String = "#bootcamp:message/skills/and/perks/text";

        public static const MESSAGE_SIX_SENSE_PERK_LABEL:String = "#bootcamp:message/six/sense/perk/label";

        public static const MESSAGE_SIX_SENSE_PERK_TEXT:String = "#bootcamp:message/six/sense/perk/text";

        public static const MESSAGE_CONSUMABLES_LABEL:String = "#bootcamp:message/consumables/label";

        public static const MESSAGE_MISSIONBONUSCOUNT:String = "#bootcamp:message/missionBonusCount";

        public static const MESSAGE_CONSUMABLES_TEXT:String = "#bootcamp:message/consumables/text";

        public static const MESSAGE_REPAIR_KIT_LABEL:String = "#bootcamp:message/repair/kit/label";

        public static const MESSAGE_FIRST_AID_KIT_LABEL:String = "#bootcamp:message/first/aid/kit/label";

        public static const MESSAGE_FIRE_EXTINGUISHER_LABEL:String = "#bootcamp:message/fire/extinguisher/label";

        public static const MESSAGE_EQUIPMENT_LABEL:String = "#bootcamp:message/equipment/label";

        public static const MESSAGE_EQUIPMENT_TEXT:String = "#bootcamp:message/equipment/text";

        public static const MESSAGE_BONUS_EQUIPMENT_LABEL:String = "#bootcamp:message/bonus/equipment/label";

        public static const MESSAGE_BONUS_EQUIPMENT_TEXT:String = "#bootcamp:message/bonus/equipment/text";

        public static const MESSAGE_GOLDRECEIVED_LABEL:String = "#bootcamp:message/goldReceived/label";

        public static const MESSAGE_GOLD_LABEL:String = "#bootcamp:message/gold/label";

        public static const MESSAGE_GOLD_PREMIUM_TEXT:String = "#bootcamp:message/gold/premium/text";

        public static const MESSAGE_GOLD_PREMIUMPLUS_TEXT:String = "#bootcamp:message/gold/premiumPlus/text";

        public static const MESSAGE_PREMIUM_LABEL:String = "#bootcamp:message/premium/label";

        public static const MESSAGE_PREMIUM_TEXT:String = "#bootcamp:message/premium/text";

        public static const MESSAGE_PREMIUMPLUS_LABEL:String = "#bootcamp:message/premiumPlus/label";

        public static const MESSAGE_PREMIUMPLUSRECEIVED_LABEL:String = "#bootcamp:message/premiumPlusReceived/label";

        public static const MESSAGE_PREMIUMPLUS_TEXT:String = "#bootcamp:message/premiumPlus/text";

        public static const MESSAGE_BONUS_PREMIUM_DAYS:String = "#bootcamp:message/bonus/premium/days";

        public static const MESSAGE_BONUS_PREMIUM_HOURS:String = "#bootcamp:message/bonus/premium/hours";

        public static const MESSAGE_MISSION_ACCOMPLISHED_LABEL:String = "#bootcamp:message/mission/accomplished/label";

        public static const MESSAGE_MISSION_ACCOMPLISHED_TEXT:String = "#bootcamp:message/mission/accomplished/text";

        public static const MESSAGE_BOOTCAMP_GRADUATE_LABEL:String = "#bootcamp:message/bootcamp/graduate/label";

        public static const MESSAGE_BOOTCAMP_GRADUATE_TEXT:String = "#bootcamp:message/bootcamp/graduate/text";

        public static const TOOLTIP_BOOTCAMP_ACHIEVEMENT_LABEL:String = "#bootcamp:tooltip/bootcamp/achievement/label";

        public static const TOOLTIP_BOOTCAMP_ACHIEVEMENT_TEXT:String = "#bootcamp:tooltip/bootcamp/achievement/text";

        public static const TOOLTIP_BOOTCAMP_BADGE_LABEL:String = "#bootcamp:tooltip/bootcamp/badge/label";

        public static const TOOLTIP_BOOTCAMP_BADGE_TEXT:String = "#bootcamp:tooltip/bootcamp/badge/text";

        public static const MESSAGE_BOOTCAMP_GRADUATE_REFERRAL_TEXT:String = "#bootcamp:message/bootcamp/graduate/referral/text";

        public static const MESSAGE_BOOTCAMP_REFERRAL_TEXT:String = "#bootcamp:message/bootcamp/referral/text";

        public static const MESSAGE_SKIP_LABEL:String = "#bootcamp:message/skip/label";

        public static const MESSAGE_SKIP_LABELEXECUTE:String = "#bootcamp:message/skip/labelExecute";

        public static const MESSAGE_SKIP_MESSAGE:String = "#bootcamp:message/skip/message";

        public static const MESSAGE_SKIP_REFERRAL_MESSAGE:String = "#bootcamp:message/skip/referral/message";

        public static const MESSAGE_SKIP_MESSAGE_PREMIUM:String = "#bootcamp:message/skip/message/premium";

        public static const MESSAGE_SKIP_MESSAGE_GOLD:String = "#bootcamp:message/skip/message/gold";

        public static const MESSAGE_SKIP_MESSAGE_CREW:String = "#bootcamp:message/skip/message/crew";

        public static const MESSAGE_SKIP_COMPLETED_MESSAGE:String = "#bootcamp:message/skip/completed/message";

        public static const MESSAGE_START_MESSAGE:String = "#bootcamp:message/start/message";

        public static const MESSAGE_START_TITLE:String = "#bootcamp:message/start/title";

        public static const MESSAGE_START_SUBMIT:String = "#bootcamp:message/start/submit";

        public static const MESSAGE_START_CANCEL:String = "#bootcamp:message/start/cancel";

        public static const MESSAGE_RESTART_MESSAGE:String = "#bootcamp:message/restart/message";

        public static const MESSAGE_RESTART_REWARD:String = "#bootcamp:message/restart/reward";

        public static const MESSAGE_RESTART_TITLE:String = "#bootcamp:message/restart/title";

        public static const MESSAGE_RESTART_SUBMIT:String = "#bootcamp:message/restart/submit";

        public static const MESSAGE_RESTART_CANCEL:String = "#bootcamp:message/restart/cancel";

        public static const RESULTLABEL_WIN:String = "#bootcamp:resultlabel/win";

        public static const RESULTLABEL_LOSE:String = "#bootcamp:resultlabel/lose";

        public static const RESULTLABEL_TIE:String = "#bootcamp:resultlabel/tie";

        public static const RESULTLABEL_DEAD:String = "#bootcamp:resultlabel/dead";

        public static const RESULTLABEL_ALIVE:String = "#bootcamp:resultlabel/alive";

        public static const RESULTLABEL_TECHWIN:String = "#bootcamp:resultlabel/techwin";

        public static const RESULTLABEL_ENDED:String = "#bootcamp:resultlabel/ended";

        public static const REWARD_LABEL:String = "#bootcamp:reward/label";

        public static const WITH_PREMIUM:String = "#bootcamp:with/premium";

        public static const BATTLE_RESULT_DESTROYED:String = "#bootcamp:battle/result/destroyed";

        public static const BATTLE_RESULT_DAMAGE:String = "#bootcamp:battle/result/damage";

        public static const BATTLE_RESULT_BLOCKED:String = "#bootcamp:battle/result/blocked";

        public static const BATTLE_RESULT_DETECTED:String = "#bootcamp:battle/result/detected";

        public static const BATTLE_RESULT_ASSISTED:String = "#bootcamp:battle/result/assisted";

        public static const BATTLE_RESULT_DESCRIPTION_DESTROYED:String = "#bootcamp:battle/result/description/destroyed";

        public static const BATTLE_RESULT_DESCRIPTION_DAMAGE:String = "#bootcamp:battle/result/description/damage";

        public static const BATTLE_RESULT_DESCRIPTION_BLOCKED:String = "#bootcamp:battle/result/description/blocked";

        public static const BATTLE_RESULT_DESCRIPTION_DETECTED:String = "#bootcamp:battle/result/description/detected";

        public static const BATTLE_RESULT_DESCRIPTION_ASSISTED:String = "#bootcamp:battle/result/description/assisted";

        public static const RESULT_AWARD_CADET_LABEL:String = "#bootcamp:result/award/cadet/label";

        public static const RESULT_AWARD_CADET_TEXT:String = "#bootcamp:result/award/cadet/text";

        public static const RESULT_AWARD_TANK_LABEL:String = "#bootcamp:result/award/tank/label";

        public static const RESULT_AWARD_TANK_TEXT:String = "#bootcamp:result/award/tank/text";

        public static const RESULT_AWARD_SNIPER_LABEL:String = "#bootcamp:result/award/sniper/label";

        public static const RESULT_AWARD_SNIPER_TEXT:String = "#bootcamp:result/award/sniper/text";

        public static const RESULT_AWARD_INVADER_LABEL:String = "#bootcamp:result/award/invader/label";

        public static const RESULT_AWARD_INVADER_TEXT:String = "#bootcamp:result/award/invader/text";

        public static const RESULT_AWARD_CREW_LABEL:String = "#bootcamp:result/award/crew/label";

        public static const RESULT_AWARD_CREW_TEXT:String = "#bootcamp:result/award/crew/text";

        public static const RESULT_AWARD_DUEL_LABEL:String = "#bootcamp:result/award/duel/label";

        public static const RESULT_AWARD_DUEL_TEXT:String = "#bootcamp:result/award/duel/text";

        public static const RESULT_AWARD_SHOOT_LABEL:String = "#bootcamp:result/award/shoot/label";

        public static const RESULT_AWARD_SHOOT_TEXT:String = "#bootcamp:result/award/shoot/text";

        public static const RESULT_AWARD_PREMIUM_LABEL:String = "#bootcamp:result/award/premium/label";

        public static const RESULT_AWARD_PREMIUM_TEXT:String = "#bootcamp:result/award/premium/text";

        public static const RESULT_AWARD_PREMIUMPLUS_LABEL:String = "#bootcamp:result/award/premiumPlus/label";

        public static const RESULT_AWARD_PREMIUMPLUS_TEXT:String = "#bootcamp:result/award/premiumPlus/text";

        public static const RESULT_AWARD_GOLD_LABEL:String = "#bootcamp:result/award/gold/label";

        public static const RESULT_AWARD_GOLD_TEXT:String = "#bootcamp:result/award/gold/text";

        public static const RESULT_AWARD_MISSION_LABEL:String = "#bootcamp:result/award/mission/label";

        public static const RESULT_AWARD_MISSION_TEXT:String = "#bootcamp:result/award/mission/text";

        public static const RESULT_AWARD_REPAIRKIT_LABEL:String = "#bootcamp:result/award/repairkit/label";

        public static const RESULT_AWARD_REPAIRKIT_TEXT:String = "#bootcamp:result/award/repairkit/text";

        public static const RESULT_AWARD_MEDICALKIT_LABEL:String = "#bootcamp:result/award/medicalkit/label";

        public static const RESULT_AWARD_MEDICALKIT_TEXT:String = "#bootcamp:result/award/medicalkit/text";

        public static const RESULT_AWARD_EXTINGUISHER_LABEL:String = "#bootcamp:result/award/extinguisher/label";

        public static const RESULT_AWARD_EXTINGUISHER_TEXT:String = "#bootcamp:result/award/extinguisher/text";

        public static const RESULT_AWARD_TOOLBOX_LABEL:String = "#bootcamp:result/award/toolbox/label";

        public static const RESULT_AWARD_TOOLBOX_TEXT:String = "#bootcamp:result/award/toolbox/text";

        public static const MESSAGE_INTRO_LESSON_II_LABEL:String = "#bootcamp:message/intro/lesson/ii/label";

        public static const MESSAGE_INTRO_LESSON_III_LABEL:String = "#bootcamp:message/intro/lesson/iii/label";

        public static const MESSAGE_INTRO_LESSON_III_CREW_LABEL:String = "#bootcamp:message/intro/lesson/iii/crew/label";

        public static const MESSAGE_INTRO_LESSON_IV_LABEL:String = "#bootcamp:message/intro/lesson/iv/label";

        public static const MESSAGE_INTRO_LESSON_V_LABEL:String = "#bootcamp:message/intro/lesson/v/label";

        public static const INVITATION_NOTE_SQUAD:String = "#bootcamp:invitation/note/squad";

        public static const INVITATION_NOTE_EVENT:String = "#bootcamp:invitation/note/event";

        public static const INVITATION_NOTE_FALLOUT:String = "#bootcamp:invitation/note/fallout";

        public static const INVITATION_NOTE_TRAINING:String = "#bootcamp:invitation/note/training";

        public static const INVITATION_NOTE_COMPANY:String = "#bootcamp:invitation/note/company";

        public static const INVITATION_NOTE_TOURNAMENT:String = "#bootcamp:invitation/note/tournament";

        public static const INVITATION_NOTE_CLAN:String = "#bootcamp:invitation/note/clan";

        public static const INVITATION_NOTE_UNIT:String = "#bootcamp:invitation/note/unit";

        public static const INVITATION_NOTE_SORTIE:String = "#bootcamp:invitation/note/sortie";

        public static const INVITATION_NOTE_FORT_BATTLE:String = "#bootcamp:invitation/note/fort/battle";

        public static const INVITATION_NOTE_CLUBS:String = "#bootcamp:invitation/note/clubs";

        public static const INVITATION_NOTE_EXTERNAL:String = "#bootcamp:invitation/note/external";

        public static const QUEUE_TITLE:String = "#bootcamp:queue/title";

        public static const QUEUE_QUEUE_TOO_LONG:String = "#bootcamp:queue/queue/too/long";

        public static const QUEUE_UNITS:String = "#bootcamp:queue/units";

        public static const QUEUE_MESSAGE:String = "#bootcamp:queue/message";

        public static const QUEUE_MORE_N_MINUTES:String = "#bootcamp:queue/more/n/minutes";

        public static const QUEUE_PLAYER_WAITING_TIME:String = "#bootcamp:queue/player-waiting-time";

        public static const QUEUE_SKIP_TUTORIAL:String = "#bootcamp:queue/skip/tutorial";

        public static const QUEUE_CANCEL_QUEUE:String = "#bootcamp:queue/cancel/queue";

        public static const TRANSITION_TITLE:String = "#bootcamp:transition/title";

        public static const BILL_TENSON:String = "#bootcamp:Bill Tenson";

        public static const BENEDIKT_DRESDNER:String = "#bootcamp:Benedikt Dresdner";

        public static const HEIKO_RIHTER:String = "#bootcamp:Heiko Rihter";

        public static const JOHN_ALBERT:String = "#bootcamp:John Albert";

        public static const DENIS_GORDIENKO:String = "#bootcamp:Denis Gordienko";

        public static const HASSO_MIRATO:String = "#bootcamp:Hasso Mirato";

        public static const RALF_HOFER:String = "#bootcamp:Ralf Hofer";

        public static const GERHARD_BRAUN:String = "#bootcamp:Gerhard Braun";

        public static const SAMUEL_BRONN:String = "#bootcamp:Samuel Bronn";

        public static const VALERIY_GAYDUCHENKO:String = "#bootcamp:Valeriy Gayduchenko";

        public static const MARK_LITENGEN:String = "#bootcamp:Mark Litengen";

        public static const ETIEN_ASIEOS:String = "#bootcamp:Etien Asieos";

        public static const ALEKSANDR_ANTONUK:String = "#bootcamp:Aleksandr Antonuk";

        public static const PETR_SERGEEV:String = "#bootcamp:Petr Sergeev";

        public static const PASCAL_RAYMOND:String = "#bootcamp:Pascal Raymond";

        public static const ALEKSEY_EGOROV:String = "#bootcamp:Aleksey Egorov";

        public static const OLIVER_GREENE:String = "#bootcamp:Oliver Greene";

        public static const JOHN_KING:String = "#bootcamp:John King";

        public static const MIRON_NEBALUIEV:String = "#bootcamp:Miron Nebaluiev";

        public static const FRIDRIH_SIMANN:String = "#bootcamp:Fridrih Simann";

        public static const MATT_UNDERLAY:String = "#bootcamp:Matt Underlay";

        public static const JAMES_BROUNGE:String = "#bootcamp:James Brounge";

        public static const ODA_NISURA:String = "#bootcamp:Oda Nisura";

        public static const GAVRIL_STOLBOV:String = "#bootcamp:Gavril Stolbov";

        public static const FABIAN_HAUPT:String = "#bootcamp:Fabian Haupt";

        public static const FRANK_DIMMELTON:String = "#bootcamp:Frank Dimmelton";

        public static const JOHN_DICKER:String = "#bootcamp:John Dicker";

        public static const KONRAD_CERSTVY:String = "#bootcamp:Konrad Cerstvy";

        public static const RICHARD_BOGELBER:String = "#bootcamp:Richard Bogelber";

        public static const KEIKO_SIMURA:String = "#bootcamp:Keiko Simura";

        public static const SHENG_EN:String = "#bootcamp:Sheng En";

        public static const SIEGWARD_EBER:String = "#bootcamp:Siegward Eber";

        public static const KARL_HIMMELSBERG:String = "#bootcamp:Karl Himmelsberg";

        public static const LEV_SHAPIRO:String = "#bootcamp:Lev Shapiro";

        public static const PAUL_BOUTIN:String = "#bootcamp:Paul Boutin";

        public static const TEODOR_SIMMERSBEE:String = "#bootcamp:Teodor Simmersbee";

        public static const CLAUD_GAULT:String = "#bootcamp:Claud Gault";

        public static const YU_DAN:String = "#bootcamp:Yu Dan";

        public static const ISIDZUKURI_SOMA:String = "#bootcamp:Isidzukuri Soma";

        public static const MITROFAN_MORDA:String = "#bootcamp:Mitrofan Morda";

        public static const YAKO_SIMAMURA:String = "#bootcamp:Yako Simamura";

        public static const LIN_SHIN:String = "#bootcamp:Lin Shin";

        public static const RADOSH_ZRVECKA:String = "#bootcamp:Radosh Zrvecka";

        public static const OTTO_VON_VALEN:String = "#bootcamp:Otto Von Valen";

        public static const VITALII_ROMANOV:String = "#bootcamp:Vitalii Romanov";

        public static const GUNTHER_FRANKE:String = "#bootcamp:Gunther Franke";

        public static const ALEKSANDR_FESICH:String = "#bootcamp:Aleksandr Fesich";

        public static const VENIAMIN_RAGOZIN:String = "#bootcamp:Veniamin Ragozin";

        public static const PAUL_KELLER:String = "#bootcamp:Paul Keller";

        public static const JING_JIE:String = "#bootcamp:Jing Jie";

        public static const JOHN_LAMB:String = "#bootcamp:John Lamb";

        public static const CORY_PRESTON:String = "#bootcamp:Cory Preston";

        public static const KARL_ERIK_OLOFSSON:String = "#bootcamp:Karl-Erik Olofsson";

        public static const ROBERT_BEASLEY:String = "#bootcamp:Robert Beasley";

        public static const JOHN_PAYNE:String = "#bootcamp:John Payne";

        public static const ELIAS_FREDRIKSSON:String = "#bootcamp:Elias Fredriksson";

        public static const JEAN_CHRISTOPHE_MOREL:String = "#bootcamp:Jean-Christophe Morel";

        public static const THOMAS_MERRITT:String = "#bootcamp:Thomas Merritt";

        public static const FEDOR_BELKIN:String = "#bootcamp:Fedor Belkin";

        public static const VLADIMIR_KAIDUN:String = "#bootcamp:Vladimir Kaidun";

        public static const PAUL_DAVIS:String = "#bootcamp:Paul Davis";

        public static const CORNELIUS_HOLST:String = "#bootcamp:Cornelius Holst";

        public static const AKENO_KIDO:String = "#bootcamp:Akeno  Kido";

        public static const ANDRII_KOZYRA:String = "#bootcamp:Andrii Kozyra";

        public static const LEE_LIANG:String = "#bootcamp:Lee Liang";

        public static const NICHOLAS_WILKINSON:String = "#bootcamp:Nicholas Wilkinson";

        public static const IGOR_GONCHARENKO:String = "#bootcamp:Igor Goncharenko";

        public static const ALEKSANDR_USTINOV:String = "#bootcamp:Aleksandr Ustinov";

        public static const YURIY_KRILO:String = "#bootcamp:Yuriy Krilo";

        public static const FUDO_SUGIMOTO:String = "#bootcamp:Fudo Sugimoto";

        public static const ALEKSEY_KLUCHIKOV:String = "#bootcamp:Aleksey Kluchikov";

        public static const CHARLES_BAKER:String = "#bootcamp:Charles Baker";

        public static const LUDVIK_BENES:String = "#bootcamp:Ludvik Benes";

        public static const JURGEN_WOLF:String = "#bootcamp:Jurgen Wolf";

        public static const JOSEPH_ONEAL:String = "#bootcamp:Joseph ONeal";

        public static const SUBTITLES_VO_BC_HANGAR_TANK_IMPROVEMENT:String = "#bootcamp:subtitles/vo_bc_hangar_tank_improvement";

        public static const SUBTITLES_VO_BC_HANGAR_EXPERIENCE:String = "#bootcamp:subtitles/vo_bc_hangar_experience";

        public static const SUBTITLES_VO_BC_HANGAR_MODULE_INVESTIGATED:String = "#bootcamp:subtitles/vo_bc_hangar_module_investigated";

        public static const SUBTITLES_VO_BC_HANGAR_SILVER:String = "#bootcamp:subtitles/vo_bc_hangar_silver";

        public static const SUBTITLES_VO_BC_HANGAR_MODULE_PURCHASED:String = "#bootcamp:subtitles/vo_bc_hangar_module_purchased";

        public static const SUBTITLES_VO_BC_HANGAR_NEW_TANK_RECEIVED:String = "#bootcamp:subtitles/vo_bc_hangar_new_tank_received";

        public static const SUBTITLES_VO_BC_HANGAR_TANK_INVESTIGATED:String = "#bootcamp:subtitles/vo_bc_hangar_tank_investigated";

        public static const SUBTITLES_VO_BC_HANGAR_TANK_IN_GARAGE:String = "#bootcamp:subtitles/vo_bc_hangar_tank_in_garage";

        public static const SUBTITLES_VO_BC_HANGAR_TEAM:String = "#bootcamp:subtitles/vo_bc_hangar_team";

        public static const SUBTITLES_VO_BC_HANGAR_SKILLS_ABILITIES:String = "#bootcamp:subtitles/vo_bc_hangar_skills_abilities";

        public static const SUBTITLES_VO_BC_HANGAR_EQUIPMENT_AND_KIT:String = "#bootcamp:subtitles/vo_bc_hangar_equipment_and_kit";

        public static const SUBTITLES_VO_BC_HANGAR_KIT:String = "#bootcamp:subtitles/vo_bc_hangar_kit";

        public static const SUBTITLES_VO_BC_HANGAR_EQUIPMENT:String = "#bootcamp:subtitles/vo_bc_hangar_equipment";

        public static const SUBTITLES_VO_BC_HANGAR_RANDOM_BATTLE:String = "#bootcamp:subtitles/vo_bc_hangar_random_battle";

        public static const SUBTITLES_VO_BC_HANGAR_GOLD_PREMIUM:String = "#bootcamp:subtitles/vo_bc_hangar_gold_premium";

        public static const SUBTITLES_VO_BC_HANGAR_GOLD:String = "#bootcamp:subtitles/vo_bc_hangar_gold";

        public static const SUBTITLES_VO_BC_HANGAR_PREMIUM:String = "#bootcamp:subtitles/vo_bc_hangar_premium";

        public static const SUBTITLES_VO_BC_HANGAR_MISSION_ACCOMPLISHED:String = "#bootcamp:subtitles/vo_bc_hangar_mission_accomplished";

        public static const SUBTITLES_VO_BC_HANGAR_CONGRATULATIONS:String = "#bootcamp:subtitles/vo_bc_hangar_congratulations";

        public function BOOTCAMP()
        {
            super();
        }
    }
}
