package
{
    public class EPIC_BATTLE extends Object
    {

        public static const STATUS_TIMELEFT:String = "#epic_battle:status/timeLeft";

        public static const TEAM1NAME:String = "#epic_battle:team1Name";

        public static const TEAM2NAME:String = "#epic_battle:team2Name";

        public static const LANE_WEST_NAME:String = "#epic_battle:lane_west_name";

        public static const LANE_CENTRAL_NAME:String = "#epic_battle:lane_central_name";

        public static const LANE_EAST_NAME:String = "#epic_battle:lane_east_name";

        public static const MISSION_TITLE:String = "#epic_battle:mission/title";

        public static const MISSION_ZONE_CLOSING_ATK:String = "#epic_battle:mission/zone_closing_atk";

        public static const MISSION_ZONE_CLOSING_DEF:String = "#epic_battle:mission/zone_closing_def";

        public static const EPICTRAINING_INFO_TEAM1LANELABEL:String = "#epic_battle:epictraining/info/team1LaneLabel";

        public static const EPICTRAINING_INFO_TEAM2LANELABEL:String = "#epic_battle:epictraining/info/team2LaneLabel";

        public static const EPICTRAINING_INFO_OTHERLABEL:String = "#epic_battle:epictraining/info/otherLabel";

        public static const ZONE_TIME_LEFT:String = "#epic_battle:zone/time_left";

        public static const ZONE_TIME_ADDED:String = "#epic_battle:zone/time_added";

        public static const ZONE_ENEMY_REINFORCED_TEXT:String = "#epic_battle:zone/enemy_reinforced_text";

        public static const ZONE_CAPTURED_TEXT:String = "#epic_battle:zone/captured_text";

        public static const ZONE_CAPTURE_TEXT:String = "#epic_battle:zone/capture_text";

        public static const ZONE_DEFEND_TEXT:String = "#epic_battle:zone/defend_text";

        public static const ZONE_DESTROY_TEXT:String = "#epic_battle:zone/destroy_text";

        public static const ZONE_REINFORCEMENTS_TEXT:String = "#epic_battle:zone/reinforcements_text";

        public static const ZONE_ARRIVED_TEXT:String = "#epic_battle:zone/arrived_text";

        public static const ZONE_HEADQUARTERS_TEXT:String = "#epic_battle:zone/headquarters_text";

        public static const ZONE_ZONE_TEXT:String = "#epic_battle:zone/zone_text";

        public static const ZONE_REACHED_TEXT:String = "#epic_battle:zone/reached_text";

        public static const ZONE_DESTROYED_TEXT:String = "#epic_battle:zone/destroyed_text";

        public static const ZONE_LOST_TEXT:String = "#epic_battle:zone/lost_text";

        public static const BASE_CONTESTED_ATK:String = "#epic_battle:base/contested_atk";

        public static const BASE_CONTESTED_DEF:String = "#epic_battle:base/contested_def";

        public static const HQ_UNDER_ATTACK_ATK:String = "#epic_battle:hq/under_attack_atk";

        public static const HQ_UNDER_ATTACK_DEF:String = "#epic_battle:hq/under_attack_def";

        public static const ZONE_REINFORCEMENT_TIMER_ATK:String = "#epic_battle:zone/reinforcement_timer_atk";

        public static const ZONE_REINFORCEMENT_TIMER_DEF:String = "#epic_battle:zone/reinforcement_timer_def";

        public static const RESPAWN_TANKS_LEFT:String = "#epic_battle:respawn/tanks_left";

        public static const RESPAWN_DEPLOY_BUTTON:String = "#epic_battle:respawn/deploy_button";

        public static const GAME_VICTORY:String = "#epic_battle:game/victory";

        public static const GAME_DEFEAT:String = "#epic_battle:game/defeat";

        public static const GAME_ALL_HQS_DESTROYED:String = "#epic_battle:game/all_hqs_destroyed";

        public static const GAME_TIME_OVER:String = "#epic_battle:game/time_over";

        public static const GAME_ALL_ENEMIES_DEFEATED:String = "#epic_battle:game/all_enemies_defeated";

        public static const GAME_ALL_TANKS_DEFEATED:String = "#epic_battle:game/all_tanks_defeated";

        public static const GAME_DRAW:String = "#epic_battle:game/draw";

        public static const CHAT_SHORTCUTS_ATTENTION_TO_POSITION:String = "#epic_battle:chat_shortcuts/attention_to_position";

        public static const CHAT_SHORTCUTS_ATTENTION_TO_OBJECTIVE_ATK:String = "#epic_battle:chat_shortcuts/attention_to_objective_atk";

        public static const CHAT_SHORTCUTS_ATTENTION_TO_OBJECTIVE_DEF:String = "#epic_battle:chat_shortcuts/attention_to_objective_def";

        public static const CHAT_SHORTCUTS_ATTENTION_TO_BASE_ATK:String = "#epic_battle:chat_shortcuts/attention_to_base_atk";

        public static const CHAT_SHORTCUTS_ATTENTION_TO_BASE_DEF:String = "#epic_battle:chat_shortcuts/attention_to_base_def";

        public static const PROGRESS_TIMERS_BASE_CAPTURE:String = "#epic_battle:progress_timers/base_capture";

        public static const PROGRESS_TIMERS_CAPTURED:String = "#epic_battle:progress_timers/captured";

        public static const PROGRESS_TIMERS_BLOCKED:String = "#epic_battle:progress_timers/blocked";

        public static const PROGRESS_TIMERS_RESUPPLY:String = "#epic_battle:progress_timers/resupply";

        public static const PROGRESS_TIMERS_ACTIVE:String = "#epic_battle:progress_timers/active";

        public static const PROGRESS_TIMERS_UNAVAILABLE:String = "#epic_battle:progress_timers/unavailable";

        public static const PROGRESS_TIMERS_FULLY_EQUIPPED:String = "#epic_battle:progress_timers/fully_equipped";

        public static const DESTROY_TIMERS_AIRSTRIKE_TXT:String = "#epic_battle:destroy_timers/airstrike_txt";

        public static const RESPAWN_AUTO_TIMER_TXT:String = "#epic_battle:respawn/auto_timer_txt";

        public static const RANK_RANK1:String = "#epic_battle:rank/rank1";

        public static const RANK_RANK2:String = "#epic_battle:rank/rank2";

        public static const RANK_RANK3:String = "#epic_battle:rank/rank3";

        public static const RANK_RANK4:String = "#epic_battle:rank/rank4";

        public static const RANK_RANK5:String = "#epic_battle:rank/rank5";

        public static const RANK_RANK6:String = "#epic_battle:rank/rank6";

        public static const RANK_NO_NAME:String = "#epic_battle:rank/no_name";

        public static const RANK_PROMOTION:String = "#epic_battle:rank/promotion";

        public static const MISSION_PRIMARY_TITLE:String = "#epic_battle:mission/primary/title";

        public static const MISSIONS_PRIMARY_ATK_BASE:String = "#epic_battle:missions/primary/atk/base";

        public static const MISSIONS_PRIMARY_DEF_BASE:String = "#epic_battle:missions/primary/def/base";

        public static const MISSIONS_PRIMARY_ATK_HQ:String = "#epic_battle:missions/primary/atk/hq";

        public static const MISSIONS_PRIMARY_DEF_HQ:String = "#epic_battle:missions/primary/def/hq";

        public static const MISSIONS_PRIMARY_ATK_HQ_SUB_TITLE:String = "#epic_battle:missions/primary/atk/hq_sub_title";

        public static const MISSIONS_PRIMARY_DEF_HQ_SUB_TITLE:String = "#epic_battle:missions/primary/def/hq_sub_title";

        public static const MISSIONS_PRIMARY_ATK_HQ_ADDITIONAL_INFO:String = "#epic_battle:missions/primary/atk/hq_additional_info";

        public static const MISSIONS_PRIMARY_DEF_HQ_ADDITIONAL_INFO:String = "#epic_battle:missions/primary/def/hq_additional_info";

        public static const MISSIONS_PRIMARY_ATK_LONGDESCRIPTION:String = "#epic_battle:missions/primary/atk/longdescription";

        public static const MISSIONS_PRIMARY_DEF_LONGDESCRIPTION:String = "#epic_battle:missions/primary/def/longdescription";

        public static const GLOBAL_MSG_SAVE_TANKS_SHORT:String = "#epic_battle:global_msg/save_tanks_short";

        public static const GLOBAL_MSG_ATK_TIME_SHORT:String = "#epic_battle:global_msg/atk/time_short";

        public static const GLOBAL_MSG_DEF_TIME_SHORT:String = "#epic_battle:global_msg/def/time_short";

        public static const GLOBAL_MSG_LANE_WEST_SHORT:String = "#epic_battle:global_msg/lane/west_short";

        public static const GLOBAL_MSG_LANE_CENTER_SHORT:String = "#epic_battle:global_msg/lane/center_short";

        public static const GLOBAL_MSG_LANE_EAST_SHORT:String = "#epic_battle:global_msg/lane/east_short";

        public static const GLOBAL_MSG_ATK_FOCUS_HQ_SHORT:String = "#epic_battle:global_msg/atk/focus_hq_short";

        public static const GLOBAL_MSG_DEF_FOCUS_HQ_SHORT:String = "#epic_battle:global_msg/def/focus_hq_short";

        public static const SPECTATOR_MODE_CAMERA_CONTROLS_TITLE:String = "#epic_battle:spectator_mode/camera_controls_title";

        public static const SPECTATOR_MODE_CAMERA_WASD:String = "#epic_battle:spectator_mode/camera_wasd";

        public static const SPECTATOR_MODE_CAMERA_MOUSE_ROTATE:String = "#epic_battle:spectator_mode/camera_mouse_rotate";

        public static const SPECTATOR_MODE_MSG_TITLE:String = "#epic_battle:spectator_mode/msg_title";

        public static const SPECTATOR_MODE_MSG_TEXT:String = "#epic_battle:spectator_mode/msg_text";

        public static const SPECTATOR_MODE_FOLLOW_TITLE:String = "#epic_battle:spectator_mode/follow_title";

        public static const SPECTATOR_MODE_FOLLOW_TEXT:String = "#epic_battle:spectator_mode/follow_text";

        public static const SPECTATOR_MODE_UNFOLLOW_TITLE:String = "#epic_battle:spectator_mode/unfollow_title";

        public static const SPECTATOR_MODE_UNFOLLOW_TEXT:String = "#epic_battle:spectator_mode/unfollow_text";

        public static const REINFORCEMENT_REINFORCEMENT_IN_TEXT:String = "#epic_battle:reinforcement/reinforcement_in_text";

        public static const REINFORCEMENT_CAPTURE_NEXT_BASE_TEXT:String = "#epic_battle:reinforcement/capture_next_base_text";

        public static const REINFORCEMENT_UNAVAILABLE:String = "#epic_battle:reinforcement/unavailable";

        public static const RECOVERY_PANEL_BASE_TEXT:String = "#epic_battle:recovery_panel/base_text";

        public static const RECOVERY_PANEL_RECOVERY_REQUESTED:String = "#epic_battle:recovery_panel/recovery_requested";

        public static const RECOVERY_PANEL_RECOVERY_CANCEL:String = "#epic_battle:recovery_panel/recovery_cancel";

        public static const RECOVERY_PANEL_RECOVERY_CANCELLED:String = "#epic_battle:recovery_panel/recovery_cancelled";

        public static const ADVANCE_MESSAGE_TXT:String = "#epic_battle:advance_message_txt";

        public static const RETREAT_MESSAGE_TXT:String = "#epic_battle:retreat_message_txt";

        public static const ADVANCE_MISSION_TXT:String = "#epic_battle:advance_mission_txt";

        public static const RETREAT_MISSION_TXT:String = "#epic_battle:retreat_mission_txt";

        public static const COMMAND_SHORTCUT_TITLE:String = "#epic_battle:command_shortcut/title";

        public static const RESPAWN_SHOP_TITLE:String = "#epic_battle:respawn/shop_title";

        public static const RESPAWN_GOLD_AMMO_NOT_AVAILABLE:String = "#epic_battle:respawn/gold_ammo_not_available";

        public static const HELP_WINDOW_RESERVES:String = "#epic_battle:help_window/reserves";

        public static const HELP_WINDOW_RESERVES_DESCRIPTION:String = "#epic_battle:help_window/reserves/description";

        public static const HELP_WINDOW_COMMUNICATION:String = "#epic_battle:help_window/communication";

        public static const HELP_WINDOW_COMMUNICATION_DESCRIPTION:String = "#epic_battle:help_window/communication/description";

        public static const HELP_WINDOW_RECOVERY:String = "#epic_battle:help_window/recovery";

        public static const HELP_WINDOW_RECOVERY_DESCRIPTION:String = "#epic_battle:help_window/recovery/description";

        public static const HELP_WINDOW_OVERVIEW_MAP:String = "#epic_battle:help_window/overview_map";

        public static const HELP_WINDOW_OVERVIEW_MAP_DESCRIPTION:String = "#epic_battle:help_window/overview_map/description";

        public static const RECOVERY_DISABLED:String = "#epic_battle:recovery/disabled";

        public static const RECOVERY_BLOCKED:String = "#epic_battle:recovery/Blocked";

        public static const SUPER_PLATOON_PANEL_TITLE:String = "#epic_battle:super_platoon_panel/title";

        public static const EPIC_BATTLE_META_VIEW_COMMANDER_LEVEL:String = "#epic_battle:epic_battle_meta_view/commander_level";

        public static const EPIC_BATTLE_META_VIEW_ABILITIES:String = "#epic_battle:epic_battle_meta_view/abilities";

        public static const EPIC_BATTLE_META_VIEW_MAX_SKILL:String = "#epic_battle:epic_battle_meta_view/Max_skill";

        public static const EPIC_BATTLE_META_VIEW_ACQUIRE_SKILL:String = "#epic_battle:epic_battle_meta_view/Acquire_skill";

        public static const OVERTIME_LABEL:String = "#epic_battle:overtime/label";

        public static const EPICBATTLESCAROUSEL_LOCKEDTOOLTIP_HEADER:String = "#epic_battle:epicBattlesCarousel/lockedTooltip/header";

        public static const EPICBATTLESCAROUSEL_LOCKEDTOOLTIP_BODY:String = "#epic_battle:epicBattlesCarousel/lockedTooltip/body";

        public static const TAB_SCREEN_SHOW_MY_LANE:String = "#epic_battle:tab_screen/show_my_lane";

        public static const TAB_SCREEN_SHOW_ALL_LANES:String = "#epic_battle:tab_screen/show_all_lanes";

        public static const IN_GAME_RANK_EXPERIENCE_TEXT:String = "#epic_battle:in_game_rank/experience_text";

        public static const ZONE_LEAVE_ZONE:String = "#epic_battle:zone/leave_zone";

        public static const METAABILITYSCREEN_UPGRADE_SKILL:String = "#epic_battle:metaAbilityScreen/Upgrade_skill";

        public static const METAABILITYSCREEN_ACQUIRE_SKILL:String = "#epic_battle:metaAbilityScreen/Acquire_skill";

        public static const METAABILITYSCREEN_ABILITY_NOT_UNLOCKED:String = "#epic_battle:metaAbilityScreen/Ability_not_unlocked";

        public static const METAABILITYSCREEN_ABILITY_LEVEL:String = "#epic_battle:metaAbilityScreen/Ability_level";

        public static const METAABILITYSCREEN_MANAGE_ABILITIES:String = "#epic_battle:metaAbilityScreen/Manage_abilities";

        public static const METAABILITYSCREEN_MANAGE_RESERVES_HEADER:String = "#epic_battle:metaAbilityScreen/Manage_reserves_header";

        public static const METAABILITYSCREEN_UNSPENT_POINTS:String = "#epic_battle:metaAbilityScreen/Unspent_points";

        public static const METAABILITYSCREEN_GET_SKILL_POINTS:String = "#epic_battle:metaAbilityScreen/get_skill_points";

        public static const SMOKE_IN_SMOKE:String = "#epic_battle:smoke/In_smoke";

        public static const ABILITYINFO_PARAMS_COMMON_COOLDOWNTIME:String = "#epic_battle:abilityInfo/params/common/cooldownTime";

        public static const ABILITYINFO_PARAMS_COMMON_DELAY:String = "#epic_battle:abilityInfo/params/common/delay";

        public static const ABILITYINFO_PARAMS_ARTILLERY_AREARADIUS:String = "#epic_battle:abilityInfo/params/artillery/areaRadius";

        public static const ABILITYINFO_PARAMS_ARTILLERY_SHOTSNUMBER:String = "#epic_battle:abilityInfo/params/artillery/shotsNumber";

        public static const ABILITYINFO_PARAMS_ARTILLERY_DURATION:String = "#epic_battle:abilityInfo/params/artillery/duration";

        public static const ABILITYINFO_PARAMS_AIRSTRIKE_DROPAREA:String = "#epic_battle:abilityInfo/params/airstrike/dropArea";

        public static const ABILITYINFO_PARAMS_AIRSTRIKE_BOMBSNUMBER:String = "#epic_battle:abilityInfo/params/airstrike/bombsNumber";

        public static const ABILITYINFO_PARAMS_AIRSTRIKE_STUN:String = "#epic_battle:abilityInfo/params/airstrike/stun";

        public static const ABILITYINFO_PARAMS_RECON_REVEALEDAREA:String = "#epic_battle:abilityInfo/params/recon/revealedArea";

        public static const ABILITYINFO_PARAMS_RECON_REVEALEDAREA_VALUE:String = "#epic_battle:abilityInfo/params/recon/revealedArea/value";

        public static const ABILITYINFO_PARAMS_RECON_SPOTTINGDURATION:String = "#epic_battle:abilityInfo/params/recon/spottingDuration";

        public static const ABILITYINFO_PARAMS_INSPIRE_RADIUS:String = "#epic_battle:abilityInfo/params/inspire/radius";

        public static const ABILITYINFO_PARAMS_INSPIRE_DURATION:String = "#epic_battle:abilityInfo/params/inspire/duration";

        public static const ABILITYINFO_PARAMS_INSPIRE_CREWINCREASEFACTOR:String = "#epic_battle:abilityInfo/params/inspire/crewIncreaseFactor";

        public static const ABILITYINFO_PARAMS_INSPIRE_INACTIVATIONDELAY:String = "#epic_battle:abilityInfo/params/inspire/inactivationDelay";

        public static const ABILITYINFO_PARAMS_SMOKE_TARGETEDAREA:String = "#epic_battle:abilityInfo/params/smoke/targetedArea";

        public static const ABILITYINFO_PARAMS_SMOKE_PROJECTILESNUMBER:String = "#epic_battle:abilityInfo/params/smoke/projectilesNumber";

        public static const ABILITYINFO_PARAMS_SMOKE_TOTALDURATION:String = "#epic_battle:abilityInfo/params/smoke/totalDuration";

        public static const ABILITYINFO_PARAMS_PASSIVE_ENGINEERING_RESUPPLYCOOLDOWNFACTOR:String = "#epic_battle:abilityInfo/params/passive_engineering/resupplyCooldownFactor";

        public static const ABILITYINFO_PARAMS_PASSIVE_ENGINEERING_RESUPPLYHEALTHPOINTSFACTOR:String = "#epic_battle:abilityInfo/params/passive_engineering/resupplyHealthPointsFactor";

        public static const ABILITYINFO_PARAMS_PASSIVE_ENGINEERING_CAPTURESPEEDFACTOR:String = "#epic_battle:abilityInfo/params/passive_engineering/captureSpeedFactor";

        public static const ABILITYINFO_PARAMS_PASSIVE_ENGINEERING_CAPTUREBLOCKBONUSTIME:String = "#epic_battle:abilityInfo/params/passive_engineering/captureBlockBonusTime";

        public static const ABILITYINFO_UNITS_METER:String = "#epic_battle:abilityInfo/units/meter";

        public static const ABILITYINFO_UNITS_SECONDS:String = "#epic_battle:abilityInfo/units/seconds";

        public static const ABILITYINFO_PROPERTIES:String = "#epic_battle:abilityInfo/properties";

        public static const ABILITYINFO_MANAGE_ABILITIES:String = "#epic_battle:abilityInfo/manage_abilities";

        public static const ABILITYINFO_MANAGE_ABILITIES_DESC:String = "#epic_battle:abilityInfo/manage_abilities_desc";

        public static const INSPIRE_INSPIRING:String = "#epic_battle:inspire/inspiring";

        public static const INSPIRE_INSPIRED:String = "#epic_battle:inspire/inspired";

        public static const SCOREPANEL_STAGE1:String = "#epic_battle:scorePanel/stage1";

        public static const SCOREPANEL_STAGE2:String = "#epic_battle:scorePanel/stage2";

        public static const SCOREPANEL_STAGE3:String = "#epic_battle:scorePanel/stage3";

        public static const REINFORCEMENTSPANEL_INTEXT:String = "#epic_battle:reinforcementsPanel/inText";

        public static const RESPAWNSCREEN_DEPLOYMENTLANE1:String = "#epic_battle:respawnScreen/deploymentLane1";

        public static const RESPAWNSCREEN_DEPLOYMENTLANE2:String = "#epic_battle:respawnScreen/deploymentLane2";

        public static const RESPAWNSCREEN_DEPLOYMENTLANE3:String = "#epic_battle:respawnScreen/deploymentLane3";

        public static const RESPAWNSCREEN_SECONDSTIMERTEXT:String = "#epic_battle:respawnScreen/secondsTimerText";

        public static const RESPAWNSCREEN_HEADERTITLE:String = "#epic_battle:respawnScreen/headerTitle";

        public static const RESPAWNSCREEN_RESPAWNWARNING:String = "#epic_battle:respawnScreen/respawnWarning";

        public static const DEPLOYMENTMAP_SPGLIMITREACHED:String = "#epic_battle:deploymentMap/spgLimitReached";

        public static const DEPLOYMENTMAP_LANEPLAYERLIMITREACHED:String = "#epic_battle:deploymentMap/lanePlayerLimitReached";

        public static const DEPLOYMENTMAP_RESPAWNWARNING:String = "#epic_battle:deploymentMap/respawnWarning";

        public static const EPICBATTLESINFOVIEW_BATTLEMODETITLE:String = "#epic_battle:epicBattlesInfoView/battleModeTitle";

        public static const EPICBATTLESINFOVIEW_MAXMETASTRING:String = "#epic_battle:epicBattlesInfoView/maxMetaString";

        public static const EPICBATTLESINFOVIEW_FAMELABELHIGHLIGHT:String = "#epic_battle:epicBattlesInfoView/fameLabelHighlight";

        public static const EPICBATTLESINFOVIEW_METALEVEL:String = "#epic_battle:epicBattlesInfoView/metaLevel";

        public static const EPICBATTLESINFOVIEW_PANELTITLE_ABILITIES:String = "#epic_battle:epicBattlesInfoView/panelTitle/abilities";

        public static const EPICBATTLESINFOVIEW_PANELTITLE_BATTLEPERFORMANCE:String = "#epic_battle:epicBattlesInfoView/panelTitle/battlePerformance";

        public static const EPICBATTLESINFOVIEW_PANELTITLE_REWARDS:String = "#epic_battle:epicBattlesInfoView/panelTitle/rewards";

        public static const EPICBATTLESINFOVIEW_PANELTITLE_PRESTIGE:String = "#epic_battle:epicBattlesInfoView/panelTitle/prestige";

        public static const EPICBATTLESINFOVIEW_PANELDESCRIPTION_ABILITIES:String = "#epic_battle:epicBattlesInfoView/panelDescription/abilities";

        public static const EPICBATTLESINFOVIEW_PANELDESCRIPTION_BATTLEPERFORMANCE:String = "#epic_battle:epicBattlesInfoView/panelDescription/battlePerformance";

        public static const EPICBATTLESINFOVIEW_PANELDESCRIPTION_REWARDS:String = "#epic_battle:epicBattlesInfoView/panelDescription/rewards";

        public static const EPICBATTLESINFOVIEW_PANELDESCRIPTION_PRESTIGE:String = "#epic_battle:epicBattlesInfoView/panelDescription/prestige";

        public static const EPICBATTLESINFOVIEW_RULEREWARDSBUTTON_LABEL:String = "#epic_battle:epicBattlesInfoView/ruleRewardsButton/label";

        public static const EPICBATTLESINFOVIEW_RULEBATTLEPERFORMANCEBUTTON_LABEL:String = "#epic_battle:epicBattlesInfoView/ruleBattlePerformanceButton/label";

        public static const EPICBATTLESINFOVIEW_RULEREWARDSPRESTIGEBUTTON_LABEL:String = "#epic_battle:epicBattlesInfoView/ruleRewardsPrestigeButton/label";

        public static const EPICBATTLESINFOVIEW_MAXLEVEL:String = "#epic_battle:epicBattlesInfoView/maxLevel";

        public static const EPICBATTLESOFFLINEVIEW_TITLE:String = "#epic_battle:epicBattlesOfflineView/title";

        public static const EPICBATTLESOFFLINEVIEW_GAMERULES:String = "#epic_battle:epicBattlesOfflineView/gameRules";

        public static const EPICBATTLESOFFLINEVIEW_CALENDARSUBTITLE:String = "#epic_battle:epicBattlesOfflineView/calendarSubTitle";

        public static const EPICBATTLESOFFLINEVIEW_CALENDARDESCRIPTION:String = "#epic_battle:epicBattlesOfflineView/calendarDescription";

        public static const EPICBATTLESWELCOMEBACKVIEW_TITLE:String = "#epic_battle:epicBattlesWelcomeBackView/title";

        public static const EPICBATTLESWELCOMEBACKVIEW_SUBTITLE:String = "#epic_battle:epicBattlesWelcomeBackView/subTitle";

        public static const EPICBATTLESWELCOMEBACKVIEW_PLAYVIDEOBTN_LABEL:String = "#epic_battle:epicBattlesWelcomeBackView/playVideoBtn/label";

        public static const EPICBATTLESPRESTIGEVIEW_PRESTIGEINFO:String = "#epic_battle:epicBattlesPrestigeView/prestigeInfo";

        public static const EPICBATTLESPRESTIGEVIEW_PRESTIGELEVEL:String = "#epic_battle:epicBattlesPrestigeView/prestigeLevel";

        public static const EPICBATTLESPRESTIGEVIEW_EXCHANGE:String = "#epic_battle:epicBattlesPrestigeView/exchange";

        public static const EPICBATTLESPRESTIGEVIEW_REMOVEABILITIES_TITLE:String = "#epic_battle:epicBattlesPrestigeView/removeAbilities/title";

        public static const EPICBATTLESPRESTIGEVIEW_RESETLEVEL_TITLE:String = "#epic_battle:epicBattlesPrestigeView/resetLevel/title";

        public static const EPICBATTLESPRESTIGEVIEW_REMOVEABILITIES_DESC:String = "#epic_battle:epicBattlesPrestigeView/removeAbilities/desc";

        public static const EPICBATTLESPRESTIGEVIEW_RESETLEVEL_DESC:String = "#epic_battle:epicBattlesPrestigeView/resetLevel/desc";

        public static const EPICBATTLESPRESTIGEVIEW_MAINTITLE:String = "#epic_battle:epicBattlesPrestigeView/mainTitle";

        public static const EPICBATTLESPRESTIGEVIEW_WARNING:String = "#epic_battle:epicBattlesPrestigeView/warning";

        public static const EPICBATTLESPRESTIGEVIEW_CONGRATULATIONS:String = "#epic_battle:epicBattlesPrestigeView/congratulations";

        public static const EPICBATTLESPRESTIGEVIEW_RESETBUTTONTEXT:String = "#epic_battle:epicBattlesPrestigeView/resetButtonText";

        public static const EPICBATTLESPRESTIGEVIEW_CANCELBUTTONTEXT:String = "#epic_battle:epicBattlesPrestigeView/cancelButtonText";

        public static const EPIC_BATTLES_AFTER_BATTLE_TITLE:String = "#epic_battle:epic_battles_after_battle/Title";

        public static const EPIC_BATTLES_AFTER_BATTLE_ACHIEVED_RANK:String = "#epic_battle:epic_battles_after_battle/Achieved_Rank";

        public static const EPIC_BATTLES_AFTER_BATTLE_LEVEL_UP_TITLE:String = "#epic_battle:epic_battles_after_battle/Level_Up_Title";

        public static const EPIC_BATTLES_AFTER_BATTLE_MAX_LEVEL_INFO_TITLE:String = "#epic_battle:epic_battles_after_battle/max_level_info/title";

        public static const EPIC_BATTLES_AFTER_BATTLE_MAX_LEVEL_INFO_DESCRIPTION:String = "#epic_battle:epic_battles_after_battle/max_level_info/description";

        public static const SELECTORTOOLTIP_EPICBATTLE_HEADER:String = "#epic_battle:selectorTooltip/epicBattle/header";

        public static const SELECTORTOOLTIP_EPICBATTLE_BODY:String = "#epic_battle:selectorTooltip/epicBattle/body";

        public static const SELECTORTOOLTIP_EPICBATTLE_FROZEN:String = "#epic_battle:selectorTooltip/epicBattle/frozen";

        public static const SELECTORTOOLTIP_EPICBATTLE_TIMETABLE_TITLE:String = "#epic_battle:selectorTooltip/epicBattle/timeTable/title";

        public static const SELECTORTOOLTIP_EPICBATTLE_TIMETABLE_TODAY:String = "#epic_battle:selectorTooltip/epicBattle/timeTable/today";

        public static const SELECTORTOOLTIP_EPICBATTLE_TIMETABLE_TOMORROW:String = "#epic_battle:selectorTooltip/epicBattle/timeTable/tomorrow";

        public static const SELECTORTOOLTIP_EPICBATTLE_TIMETABLE_DASH:String = "#epic_battle:selectorTooltip/epicBattle/timeTable/dash";

        public static const SELECTORTOOLTIP_EPICBATTLE_TIMELEFT:String = "#epic_battle:selectorTooltip/epicBattle/timeLeft";

        public static const SELECTORTOOLTIP_EPICBATTLE_STARTIN:String = "#epic_battle:selectorTooltip/epicBattle/startIn";

        public static const SELECTORTOOLTIP_EPICBATTLE_ATTENTION_ASSUREDLOWPERFORMANCE_TITLE:String = "#epic_battle:selectorTooltip/epicBattle/attention/assuredLowPerformance/title";

        public static const SELECTORTOOLTIP_EPICBATTLE_ATTENTION_ASSUREDLOWPERFORMANCE_DESCRIPTION:String = "#epic_battle:selectorTooltip/epicBattle/attention/assuredLowPerformance/description";

        public static const SELECTORTOOLTIP_EPICBATTLE_ATTENTION_POSSIBLELOWPERFORMANCE_TITLE:String = "#epic_battle:selectorTooltip/epicBattle/attention/possibleLowPerformance/title";

        public static const SELECTORTOOLTIP_EPICBATTLE_ATTENTION_POSSIBLELOWPERFORMANCE_DESCRIPTION:String = "#epic_battle:selectorTooltip/epicBattle/attention/possibleLowPerformance/description";

        public static const SELECTORTOOLTIP_EPICBATTLE_ATTENTION_INFORMATIVELOWPERFORMANCE_TITLE:String = "#epic_battle:selectorTooltip/epicBattle/attention/informativeLowPerformance/title";

        public static const SELECTORTOOLTIP_EPICBATTLE_ATTENTION_INFORMATIVELOWPERFORMANCE_DESCRIPTION:String = "#epic_battle:selectorTooltip/epicBattle/attention/informativeLowPerformance/description";

        public static const QUESTSTOOLTIP_EPICBATTLE_HEADER:String = "#epic_battle:questsTooltip/epicBattle/header";

        public static const QUESTSTOOLTIP_EPICBATTLE_TIMELEFT:String = "#epic_battle:questsTooltip/epicBattle/timeLeft";

        public static const QUESTSTOOLTIP_EPICBATTLE_STARTIN:String = "#epic_battle:questsTooltip/epicBattle/startIn";

        public static const QUESTSTOOLTIP_EPICBATTLE_LESSTHANDAY:String = "#epic_battle:questsTooltip/epicBattle/lessThanDay";

        public static const QUESTSTOOLTIP_EPICBATTLE_UNAVAILABLE:String = "#epic_battle:questsTooltip/epicBattle/unavailable";

        public static const QUESTSTOOLTIP_EPICBATTLE_RESTRICT_LEVEL:String = "#epic_battle:questsTooltip/epicBattle/restrict/level";

        public static const WIDGETALERTMESSAGEBLOCK_NOCYCLEMESSAGE:String = "#epic_battle:widgetAlertMessageBlock/noCycleMessage";

        public static const WIDGETALERTMESSAGEBLOCK_ALLPERIPHERIESHALT:String = "#epic_battle:widgetAlertMessageBlock/allPeripheriesHalt";

        public static const WIDGETALERTMESSAGEBLOCK_SOMEPERIPHERIESHALT:String = "#epic_battle:widgetAlertMessageBlock/somePeripheriesHalt";

        public static const WIDGETALERTMESSAGEBLOCK_SINGLEMODEHALT:String = "#epic_battle:widgetAlertMessageBlock/singleModeHalt";

        public static const WIDGETALERTMESSAGEBLOCK_BUTTON:String = "#epic_battle:widgetAlertMessageBlock/button";

        public static const WIDGETALERTMESSAGEBLOCK_STARTIN:String = "#epic_battle:widgetAlertMessageBlock/startIn";

        public static const STATUS_TIMELEFT_DAYS:String = "#epic_battle:status/timeLeft/days";

        public static const STATUS_TIMELEFT_HOURS:String = "#epic_battle:status/timeLeft/hours";

        public static const STATUS_TIMELEFT_MIN:String = "#epic_battle:status/timeLeft/min";

        public static const STATUS_TIMELEFT_LESSMIN:String = "#epic_battle:status/timeLeft/lessMin";

        public static const BATTLERESULTS_SPOTTING:String = "#epic_battle:battleResults/spotting";

        public static const BATTLERESULTS_ASSISTANCE:String = "#epic_battle:battleResults/assistance";

        public static const BATTLERESULTS_ARMOR:String = "#epic_battle:battleResults/armor";

        public static const BATTLERESULTS_CRITS:String = "#epic_battle:battleResults/crits";

        public static const BATTLERESULTS_KILL:String = "#epic_battle:battleResults/kill";

        public static const BATTLERESULTS_DAMAGE:String = "#epic_battle:battleResults/damage";

        public static const METAABILITYSCREEN_ABILITY_MAX_LEVEL:String = "#epic_battle:metaAbilityScreen/Ability_max_level";

        public static const METAABILITYSCREEN_ABILITY_LOCKED:String = "#epic_battle:metaAbilityScreen/Ability_locked";

        public static const METAABILITYSCREEN_ABILITY_NOT_POINTS:String = "#epic_battle:metaAbilityScreen/Ability_not_points";

        public static const BOOSTER_DESCRIPTION_BONUSVALUETIME_BOOSTER_XP:String = "#epic_battle:booster/description/bonusValueTime/booster_xp";

        public static const BOOSTER_DESCRIPTION_BONUSVALUETIME_BOOSTER_FREE_XP:String = "#epic_battle:booster/description/bonusValueTime/booster_free_xp";

        public static const BOOSTER_DESCRIPTION_BONUSVALUETIME_BOOSTER_CREW_XP:String = "#epic_battle:booster/description/bonusValueTime/booster_crew_xp";

        public static const FITTINGSELECTPOPOVER_REMOVEBUTTON:String = "#epic_battle:fittingSelectPopover/RemoveButton";

        public static const FITTINGSELECTPOPOVER_CHANGEORDER:String = "#epic_battle:fittingSelectPopover/ChangeOrder";

        public static const FITTINGSELECTPOPOVER_NOTACTIVATED:String = "#epic_battle:fittingSelectPopover/notActivated";

        public static const FITTINGSELECTPOPOVERBATTKEABILITY_TITLETEXT:String = "#epic_battle:fittingSelectPopoverBattkeAbility/titleText";

        public static const FITTINGSELECTPOPOVERBATTKEABILITY_DESCTEXT:String = "#epic_battle:fittingSelectPopoverBattkeAbility/descText";

        public static const PRIMETIME_TITLE:String = "#epic_battle:primeTime/title";

        public static const PRIMETIME_TITLEWELCOME:String = "#epic_battle:primeTime/titleWelcome";

        public static const PRIMETIME_APPLYBTN:String = "#epic_battle:primeTime/applyBtn";

        public static const PRIMETIME_CANCELBTN:String = "#epic_battle:primeTime/cancelBtn";

        public static const PRIMETIME_CONTINUEBTN:String = "#epic_battle:primeTime/continueBtn";

        public static const PRIMETIME_SERVERTOOLTIP:String = "#epic_battle:primeTime/serverTooltip";

        public static const PRIMETIME_SERVERUNAVAILABLETOOLTIP:String = "#epic_battle:primeTime/serverUnavailableTooltip";

        public static const PRIMETIME_SERVERS:String = "#epic_battle:primeTime/servers";

        public static const PRIMETIME_STATUS_NOPRIMETIMEONTHISSERVER:String = "#epic_battle:primeTime/status/noPrimeTimeOnThisServer";

        public static const PRIMETIME_STATUS_CYCLEFINISHEDONTHISSERVER:String = "#epic_battle:primeTime/status/cycleFinishedOnThisServer";

        public static const PRIMETIME_STATUS_NOPRIMETIMESONALLSERVERS:String = "#epic_battle:primeTime/status/noPrimeTimesOnAllServers";

        public static const PRIMETIME_STATUS_DISABLEDONTHISSERVER:String = "#epic_battle:primeTime/status/disabledOnThisServer";

        public static const PRIMETIME_MANYSERVERSAVAILABLE:String = "#epic_battle:primeTime/manyServersAvailable";

        public static const PRIMETIME_ONESERVERAVAILABLE:String = "#epic_battle:primeTime/oneServerAvailable";

        public static const PRIMETIME_PRIMEISAVAILABLE:String = "#epic_battle:primeTime/primeIsAvailable";

        public static const PRIMETIME_PRIMEWILLBEAVAILABLE:String = "#epic_battle:primeTime/primeWillBeAvailable";

        public static const PRIMETIME_ENDOFCYCLE:String = "#epic_battle:primeTime/endOfCycle";

        public static const PRIMETIME_STATUS_THISENABLE:String = "#epic_battle:primeTime/status/thisEnable";

        public static const PRIMETIME_STATUS_DISABLE:String = "#epic_battle:primeTime/status/disable";

        public static const TUTORIAL_HINT_EPICRESERVESSLOTHINT:String = "#epic_battle:tutorial/hint/epicReservesSlotHint";

        public static const TUTORIAL_HINT_EPICRESERVESBTNHINT:String = "#epic_battle:tutorial/hint/epicReservesBtnHint";

        public static const SEASON_201902_NAME:String = "#epic_battle:season/201902/name";

        public static const SEASON_202002_NAME:String = "#epic_battle:season/202002/name";

        public static const EPICBATTLEITEM_REWARDPOINTS_HEADER:String = "#epic_battle:epicBattleItem/rewardPoints/header";

        public static const EPICBATTLEITEM_REWARDPOINTS_DESCRIPTION:String = "#epic_battle:epicBattleItem/rewardPoints/description";

        public static const EPICBATTLEITEM_SUPPLYPOINTS_HEADER:String = "#epic_battle:epicBattleItem/supplyPoints/header";

        public static const EPICBATTLEITEM_SUPPLYPOINTS_DESCRIPTION:String = "#epic_battle:epicBattleItem/supplyPoints/description";

        public function EPIC_BATTLE()
        {
            super();
        }
    }
}
