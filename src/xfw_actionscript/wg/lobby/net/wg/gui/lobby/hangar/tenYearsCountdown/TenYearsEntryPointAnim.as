package net.wg.gui.lobby.hangar.tenYearsCountdown
{
    import flash.display.MovieClip;

    public class TenYearsEntryPointAnim extends MovieClip
    {

        public function TenYearsEntryPointAnim()
        {
            super();
            mouseChildren = mouseEnabled = false;
        }
    }
}
