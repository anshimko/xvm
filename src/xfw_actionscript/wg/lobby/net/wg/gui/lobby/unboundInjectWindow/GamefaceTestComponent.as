package net.wg.gui.lobby.unboundInjectWindow
{
    import net.wg.gui.components.containers.inject.GFInjectComponent;

    public class GamefaceTestComponent extends GFInjectComponent
    {

        public function GamefaceTestComponent()
        {
            super();
        }
    }
}
