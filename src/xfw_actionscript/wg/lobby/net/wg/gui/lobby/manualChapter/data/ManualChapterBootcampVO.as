package net.wg.gui.lobby.manualChapter.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class ManualChapterBootcampVO extends DAAPIDataClass
    {

        public var text:String = "";

        public function ManualChapterBootcampVO(param1:Object = null)
        {
            super(param1);
        }
    }
}
